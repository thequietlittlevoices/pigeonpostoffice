$ ->
  $('select#pigeon_country').change (event) ->
    select_wrapper = $('#pigeon_subregion_wrapper')

    $('select', select_wrapper).attr('disabled', true)

    country = $(this).val()

    url = "/subregion_options?parent_region=#{country}"
    select_wrapper.load(url)

    $('select#locale').change (event) ->
      $(@).closest('form').submit()


