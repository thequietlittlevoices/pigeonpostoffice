class Pigeon

  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  
  attr_accessor :first_name, :last_name, :email, :country, :subregion
 
  validates_presence_of :first_name, :last_name, :email, :country, :subregion
  validates_format_of :first_name, :last_name, :with => /\A[a-zA-Z]+\z/i, :message => "should contain only letters"
  validates_format_of :email, :with => /\A[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}\z/i, :message =>"should have a valid format"
  validates_confirmation_of :email

  # loop through each item in hash and assign the value to each of the attributes of the message
  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end if attributes
  end
  
  def persisted?
    false
  end
end
