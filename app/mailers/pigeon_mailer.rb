class PigeonMailer < ActionMailer::Base
  default from: "timea.retek@gmail.com"

  def send_email(first_name, last_name, email, country, subregion)
    email_body = "#{first_name} #{last_name}, from country #{country} (subregion #{subregion}), is trying to reach you through pigeon post."
    mail(to: email, body: email_body, subject: "The pigeon has arrived.")

  end
end
