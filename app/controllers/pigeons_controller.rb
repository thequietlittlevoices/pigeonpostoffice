class PigeonsController < ApplicationController


  def new
    @pigeon = Pigeon.new

  end

  def create
    @pigeon = Pigeon.new(params[:pigeon])

    if @pigeon.valid?
      session[:first_name] = @pigeon.first_name
      session[:last_name] = @pigeon.last_name
      session[:email] = @pigeon.email
      session[:country] = @pigeon.country
      session[:subregion] = @pigeon.subregion
      render :show
    else
       render :new
    end
  end

  def deliver

    PigeonMailer.send_email(session[:first_name], session[:last_name], session[:email], session[:country], session[:subregion]).deliver
    flash[:notice] = 'Message on the way!'
    redirect_to :action => :new 
  end

  def subregion_options
    render partial: 'subregion_select'
  end
end