module PigeonHelper
  def pigeon_error_messages!
    return "" if @pigeon.errors.empty?

    messages = @pigeon.errors.full_messages.map { |msg| content_tag(:li, msg) }.join

    html = <<-HTML
    <div id="alert alert-danger">
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end
end